package com.yue.ar.suite;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class ARSuite extends ApplicationAdapter {
	private static final String TAG = ARSuite.class.getSimpleName();

	// 紋理
	Texture texture;

	// 演員
	private MyActor actor;

	// 舞台
	private Stage stage;

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		texture = new Texture(Gdx.files.internal("badlogic.jpg"));
		actor = new MyActor(new TextureRegion(texture));

		actor.setPosition(20,40);

		stage = new Stage();

		stage.addActor(actor);

		Gdx.input.setInputProcessor(new MyInputProcesser());

		stage.addListener(new MyClickListener());

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();

		stage.draw();

	}
	
	@Override
	public void dispose () {

		if(texture != null){
			texture.dispose();
		}

		if(stage != null){
			stage.dispose();
		}


	}
}
