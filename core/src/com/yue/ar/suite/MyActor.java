package com.yue.ar.suite;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by luokangyu on 2017/9/21.
 */

public class MyActor extends Actor{

    private TextureRegion region;

    public MyActor(TextureRegion region){

        super();

        this.region = region;

        setSize(this.region.getRegionWidth(),this.region.getRegionHeight());

    }


    public TextureRegion getRegion(){
        return  region;
    }

    public void act(float delta){
        super.act(delta);
    }

    public void draw(Batch batch, float presentAlpha){

        super.draw(batch,presentAlpha);

        if(region == null || !isVisible()){
            return;
        }

        batch.draw(region,
                getX(),getY(),
                getOriginX(),getOriginY(),
                getWidth(),getHeight(),
                getScaleX(),getScaleY(),
                getRotation()
        );


    }



}
